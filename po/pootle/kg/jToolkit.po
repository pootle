msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-06-13 14:54-0500\n"
"PO-Revision-Date: 2006-11-20 23:58+0100\n"
"Last-Translator: Nsiangani Kusimbiko <kibavuidi.nsiangani@free.fr>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 0.10.1\n"

#: web/server.py:57
#, python-format
msgid "Login for %s"
msgstr "wizidi tuka mu %s"

#: web/server.py:91
msgid "Cancel this action and start a new session"
msgstr "yambula salu kiokio ye kota mu velumbu kia mpa"

#: web/server.py:92
msgid "Instead of confirming this action, log out and start from scratch"
msgstr "ku tambula salu kiokio ko, kansi vayika ye yantika diaka"

#: web/server.py:97
msgid "Exit application"
msgstr "sisa lusalulu"

#: web/server.py:98
msgid "Exit this application and return to the parent application"
msgstr "sisa lusalulu yoyo ye vutuka kua mbuta lusalulu"

#: web/server.py:105
msgid ", please confirm login"
msgstr "kotele kua ku e?"

#: web/server.py:110
msgid "Login"
msgstr "Nkotolo"

#: web/server.py:123
msgid "Username:"
msgstr "zina"

#: web/server.py:125
msgid "Password:"
msgstr "mvovo nsabi:"

#: web/server.py:130
msgid "Language:"
msgstr "ndinga"

#: web/server.py:551 web/server.py:642
msgid "Select Application"
msgstr "sola lusalulu"

#: web/server.py:637
msgid "Logout"
msgstr "vayika"

#: web/server.py:639
msgid "Exit"
msgstr "Manisa"

#: web/session.py:191 web/session.py:344 web/session.py:350
#, python-format
msgid "logged in as <b>%s</b>"
msgstr "Kotele, nkumbu: <b>%s</b>"

#: web/session.py:193
msgid "couldn't add new session"
msgstr "n'a pas pu ajouter une nouvelle session"

#: web/session.py:214
msgid "logged out"
msgstr "vayikidi"

#: web/session.py:234
msgid "connected"
msgstr "kotele"

#: web/session.py:269
msgid "exited parent session"
msgstr "mbuta velumbu sisidi yo"

#: web/session.py:314
msgid "need to define the users or userprefs entry on the server config"
msgstr "fueti sola basadi ovo venkadilu zau ku nsingik'a vesadi."

#: web/session.py:321 web/session.py:453
#, python-format
msgid "user does not exist (%r)"
msgstr "(%r) kenani ko"

#: web/session.py:362
msgid "invalid username and/or password"
msgstr "zina/mvovo nsabi kena ya mbote ko"

#: web/session.py:433 web/session.py:437
msgid "failed login confirmation"
msgstr "Échec de la confirmation du Login"

#: widgets/form.py:79 widgets/thumbgallery.py:292
msgid "No"
msgstr "nkatu"

#: widgets/form.py:80 widgets/thumbgallery.py:293
msgid "Yes"
msgstr "inga"

#: widgets/thumbgallery.py:233 attachments.py:348
msgid "remove"
msgstr "katula"

#: widgets/thumbgallery.py:289 attachments.py:322
msgid "(no attachment)"
msgstr "(ka kima ko)"

#: attachments.py:70
msgid "attachment is too large"
msgstr "kima kia kinene kibeni"

#: attachments.py:349
msgid "remove this attachment"
msgstr "katula kima kiokio"

#: attachments.py:350
msgid "restore"
msgstr "yantika diaka"

#: attachments.py:351
msgid "this attachment has been removed. click here to restore it"
msgstr "kima kiokio vunzamene, fina fulu kiku mu vutukila kio"

#: attachments.py:362
msgid "attach a file"
msgstr "kotesa konso kima"
