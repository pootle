msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: translate-pootle@lists.sourceforge.net\n"
"POT-Creation-Date: 2007-04-13 12:24+0200\n"
"PO-Revision-Date: 2007-10-28 19:17+0100\n"
"Last-Translator: Igor Pedro Gonçalves Gil <igorpgil@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 1.0.2\n"
"Generated-By: pygettext.py 1.1\n"

#. l10n: Verb
#: projects.py:136
msgid "View"
msgstr "Ver"

#: projects.py:137 translatepage.py:613
msgid "Suggest"
msgstr "Sugerir"

#: projects.py:138
msgid "Translate"
msgstr "Traduzir"

#. l10n: radio button text
#: projects.py:139 indexpage.py:548
msgid "Overwrite"
msgstr "Sobreescrever"

#. l10n: Verb
#: projects.py:141
msgid "Review"
msgstr "Rever"

#. l10n: Verb
#: projects.py:143
msgid "Archive"
msgstr "Arquivar"

#. l10n: This refers to generating the binary .mo file
#: projects.py:145
msgid "Compile PO files"
msgstr "Compilar ficheiros PO"

#: projects.py:146
msgid "Assign"
msgstr "Atribuir"

#: projects.py:147 indexpage.py:134
msgid "Administrate"
msgstr "Administrar"

#. l10n: Commit to version control (like CVS or Subversion)
#: projects.py:148 indexpage.py:748
msgid "Commit"
msgstr "Submeter"

#. l10n: Don't translate "nobody" or "default"
#: projects.py:228
msgid "You cannot remove the \"nobody\" or \"default\" user"
msgstr "Você não pode remover os utilizadores \"nobody\" ou \"default\""

#: projects.py:380 projects.py:442
msgid "You do not have rights to alter goals here"
msgstr "Você não tem permissões para alterar metas aqui"

#: projects.py:505
msgid "You do not have rights to upload files here"
msgstr "Você não tem permissões para enviar ficheiros aqui"

#: projects.py:508
msgid "You do not have rights to overwrite files here"
msgstr "Você não tem permissões para sobreescrever ficheiros aqui"

#: projects.py:510
msgid "You do not have rights to upload new files here"
msgstr "Você não tem permissões para enviar novos ficheiros aqui"

#: projects.py:519
msgid "You do not have rights to update files here"
msgstr "Você não tem permissões para actualizar ficheiros aqui"

#: projects.py:590
msgid "You do not have rights to commit files here"
msgstr "Você não tem permissões para submeter ficheiros aqui"

#: projects.py:919 projects.py:960
msgid "You do not have rights to alter assignments here"
msgstr "Você não tem permissões para alterar designações aqui"

#: projects.py:1141
msgid "You do not have rights to change translations here"
msgstr "Você não tem permissões para mudar traduções aqui"

#: projects.py:1151
msgid "You do not have rights to suggest changes here"
msgstr "Você não tem permissões para sugerir mudanças aqui"

#: projects.py:1167 projects.py:1185
msgid "You do not have rights to review suggestions here"
msgstr "Você não tem permissões para rever sugestões aqui"

#: translatepage.py:89
#, python-format
msgid ""
"%d/%d translated\n"
"(%d blank, %d fuzzy)"
msgstr ""
"%d/%d traduzidas\n"
"(%d em branco, %d duvidosas)"

#: translatepage.py:94 adminpages.py:35 adminpages.py:74 adminpages.py:132
#: adminpages.py:197 adminpages.py:284 adminpages.py:344 pagelayout.py:72
#: users.py:58 users.py:95 users.py:131 users.py:153 indexpage.py:73
#: indexpage.py:96 indexpage.py:136 indexpage.py:194 indexpage.py:256
#: indexpage.py:346
msgid "Pootle Demo"
msgstr "Demonstração do Pootle"

#. l10n: first parameter: name of the installation (like "Pootle")
#. l10n: second parameter: project name
#. l10n: third parameter: target language
#. l10n: fourth parameter: file name
#: translatepage.py:99
#, python-format
msgid "%s: translating %s into %s: %s"
msgstr "%s: a traduzir %s para %s: %s"

#. l10n: Heading above the table column with the source language
#: translatepage.py:114
msgid "Original"
msgstr "Original"

#. l10n: Heading above the table column with the target language
#: translatepage.py:116
msgid "Translation"
msgstr "Tradução"

#: translatepage.py:119
msgid "Accept"
msgstr "Aceitar"

#: translatepage.py:120
msgid "Reject"
msgstr "Rejeitar"

#: translatepage.py:121 pagelayout.py:243
msgid "Fuzzy"
msgstr "Tradução aproximada (fuzzy)"

#. l10n: Heading above the textarea for translator comments.
#: translatepage.py:123
msgid "Translator comments"
msgstr "Comentários do tradutor"

#. l10n: Heading above the comments extracted from the programing source code
#: translatepage.py:125
msgid "Developer comments"
msgstr "Comentários do programador"

#. l10n: This heading refers to related translations and terminology
#: translatepage.py:127
msgid "Related"
msgstr "Relacionado"

#. l10n: text next to search field
#: translatepage.py:131 indexpage.py:356
msgid "Search"
msgstr "Procurar"

#. l10n: "batch" refers to the set of translations that were reviewed
#: translatepage.py:145
msgid "End of batch"
msgstr "Fim do lote de traduções"

#: translatepage.py:147
msgid "Click here to return to the index"
msgstr "Clique aqui para voltar para o índice"

#. l10n: noun (the start)
#: translatepage.py:161 translatepage.py:164
msgid "Start"
msgstr "Início"

#. l10n: the parameter refers to the number of messages
#: translatepage.py:168 translatepage.py:171
#, python-format
msgid "Previous %d"
msgstr "%d anteriores"

#. l10n: the third parameter refers to the total number of messages in the file
#: translatepage.py:173
#, python-format
msgid "Items %d to %d of %d"
msgstr "Itens %d para %d de %d"

#. l10n: the parameter refers to the number of messages
#: translatepage.py:178 translatepage.py:181
#, python-format
msgid "Next %d"
msgstr "Próximos %d"

#. l10n: noun (the end)
#: translatepage.py:184 translatepage.py:187
msgid "End"
msgstr "Fim"

#. l10n: the parameter is the name of one of the quality checks, like "fuzzy"
#: translatepage.py:204
#, python-format
msgid "checking %s"
msgstr "a verificar %s"

#: translatepage.py:211 translatepage.py:214 indexpage.py:526 indexpage.py:529
msgid "Assign Strings"
msgstr "Atribuir Strings"

#: translatepage.py:212 indexpage.py:528
msgid "Assign to User"
msgstr "Atribuir ao Utilizador"

#: translatepage.py:213 indexpage.py:527
msgid "Assign Action"
msgstr "Atribuir Acção"

#: translatepage.py:368
#, python-format
msgid "There are no items matching that search ('%s')"
msgstr "Nenhum item coincide com a busca ('%s') "

#: translatepage.py:370
msgid "You have finished going through the items you selected"
msgstr "Acabou de verificar todos os itens que seleccionou"

#: translatepage.py:578
msgid "Singular"
msgstr "Singular"

#: translatepage.py:579
msgid "Plural"
msgstr "Plural"

#. l10n: verb
#: translatepage.py:593
msgid "Edit"
msgstr "Editar"

#. l10n: verb
#: translatepage.py:609
msgid "Copy"
msgstr "Copiar"

#: translatepage.py:610 translatepage.py:784
msgid "Skip"
msgstr "Saltar"

#. l10n: verb
#: translatepage.py:612 translatepage.py:783 users.py:256
msgid "Back"
msgstr "Atrás"

#: translatepage.py:614
msgid "Submit"
msgstr "Submeter"

#. l10n: action that increases the height of the textarea
#: translatepage.py:617
msgid "Grow"
msgstr "Aumentar"

#. l10n: action that decreases the height of the textarea
#: translatepage.py:619
msgid "Shrink"
msgstr "Encolher"

#: translatepage.py:641 translatepage.py:735 translatepage.py:772
#: translatepage.py:805
#, python-format
msgid "Plural Form %d"
msgstr "Forma Plural %d"

#. l10n: This is an error message that will display if the relevant problem occurs
#: translatepage.py:657
msgid ""
"Translation not possible because plural information for your language is not "
"available. Please contact the site administrator."
msgstr ""
"A tradução não é possível porque não está disponível a informação de plural "
"para o seu idioma. Por favor contacte o admnistrador do site."

#: translatepage.py:739
msgid "Current Translation:"
msgstr "Tradução Actual:"

#. l10n: First parameter: number
#. l10n: Second parameter: name of translator
#: translatepage.py:752
#, python-format
msgid "Suggestion %d by %s:"
msgstr "Sugestão %d por %s:"

#: translatepage.py:754
#, python-format
msgid "Suggestion %d:"
msgstr "Sugestão %d:"

#. l10n: parameter: name of translator
#: translatepage.py:758
#, python-format
msgid "Suggestion by %s:"
msgstr "Sugerido por %s:"

#: translatepage.py:760
msgid "Suggestion:"
msgstr "Sugestão:"

#: adminpages.py:43 adminpages.py:82 adminpages.py:140 adminpages.py:205
#: pagelayout.py:36
msgid "Home"
msgstr "Início"

#: adminpages.py:44 adminpages.py:207
msgid "Users"
msgstr "Utilizadores"

#: adminpages.py:45 adminpages.py:84 indexpage.py:94
msgid "Languages"
msgstr "Idiomas"

#: adminpages.py:46 adminpages.py:142 indexpage.py:95
msgid "Projects"
msgstr "Projectos"

#: adminpages.py:47
msgid "General options"
msgstr "Opções gerais"

#: adminpages.py:48 users.py:157
msgid "Option"
msgstr "Opção"

#: adminpages.py:49 users.py:158
msgid "Current value"
msgstr "Valor actual"

#: adminpages.py:50 adminpages.py:85 adminpages.py:143 adminpages.py:208
#: users.py:172
msgid "Save changes"
msgstr "Guardar alterações"

#: adminpages.py:54
msgid "Title"
msgstr "Título"

#: adminpages.py:55
msgid "Description"
msgstr "Descrição"

#: adminpages.py:56
msgid "Base URL"
msgstr "URL Base"

#: adminpages.py:57
msgid "Home Page"
msgstr "Página inicial"

#: adminpages.py:83 adminpages.py:141 adminpages.py:206
msgid "Main admin page"
msgstr "Página principal de administração"

#: adminpages.py:89
msgid "ISO Code"
msgstr "Código ISO"

#: adminpages.py:90 adminpages.py:148 adminpages.py:213 users.py:104
msgid "Full Name"
msgstr "Nome Completo"

#: adminpages.py:91
msgid "(add language here)"
msgstr "(adicionar idioma aqui)"

#: adminpages.py:92
msgid "Special Chars"
msgstr "Caracteres Especiais"

#: adminpages.py:92
msgid "(special characters)"
msgstr "(caracteres especiais)"

#: adminpages.py:93 indexpage.py:217
msgid "Number of Plurals"
msgstr "Número de Plurais"

#: adminpages.py:93
msgid "(number of plurals)"
msgstr "(número de plurais)"

#: adminpages.py:94 indexpage.py:218
msgid "Plural Equation"
msgstr "Formação de Plural"

#: adminpages.py:94
msgid "(plural equation)"
msgstr "(formação de equação)"

#: adminpages.py:95
msgid "Remove Language"
msgstr "Remover Idioma"

#. l10n: The parameter is a languagecode, projectcode or username
#: adminpages.py:110 adminpages.py:177 adminpages.py:240 adminpages.py:410
#, python-format
msgid "Remove %s"
msgstr "Remover %s"

#: adminpages.py:129
msgid "Standard"
msgstr "Padrão"

#: adminpages.py:147
msgid "Project Code"
msgstr "Código do Projecto"

#: adminpages.py:149
msgid "(add project here)"
msgstr "(adicionar projecto aqui)"

#: adminpages.py:150
msgid "Project Description"
msgstr "Descrição do Projecto"

#: adminpages.py:150
msgid "(project description)"
msgstr "(descrição do projecto)"

#: adminpages.py:151
msgid "Checker Style"
msgstr "Estilo do Verificador"

#: adminpages.py:152
msgid "File Type"
msgstr "Tipo de ficheiro"

#: adminpages.py:153
msgid "Create MO Files"
msgstr "Criar Ficheiros MO"

#: adminpages.py:154
msgid "Remove Project"
msgstr "Remover Projecto"

#: adminpages.py:212 users.py:66
msgid "Login"
msgstr "Identificação"

#: adminpages.py:214 users.py:347
msgid "(add full name here)"
msgstr "(adicionar nome completo aqui)"

#: adminpages.py:215 users.py:101
msgid "Email Address"
msgstr "Endereço de email"

#: adminpages.py:215 users.py:350
msgid "(add email here)"
msgstr "(adicionar email aqui)"

#: adminpages.py:216 users.py:107 users.py:163
msgid "Password"
msgstr "Senha"

#: adminpages.py:216 users.py:344
msgid "(add password here)"
msgstr "(adicionar senha aqui)"

#: adminpages.py:217
msgid "Activated"
msgstr "Activado"

#: adminpages.py:217
msgid "Activate New User"
msgstr "Activar Novo Utilizador"

#: adminpages.py:218
msgid "Remove User"
msgstr "Remover Utilizador"

#: adminpages.py:272
msgid "Back to main page"
msgstr "Voltar à página principal"

#: adminpages.py:273
msgid "Existing languages"
msgstr "Idiomas existentes"

#. l10n: This refers to updating the translation files from the templates like with .pot files
#: adminpages.py:277
msgid "Update Languages"
msgstr "Actualizar idiomas"

#: adminpages.py:278
#, python-format
msgid "Pootle Admin: %s"
msgstr "Administrador do Pootle: %s"

#: adminpages.py:279 adminpages.py:341
msgid "You do not have the rights to administer this project."
msgstr "Você não tem o direito de administrar este projeto."

#. l10n: This refers to updating the translation files from the templates like with .pot files
#: adminpages.py:281
msgid "Update from templates"
msgstr "Actualizar pelos modelos"

#: adminpages.py:289
msgid "Add Language"
msgstr "Adicionar idioma"

#. l10n: This is the page title. The first parameter is the language name, the second parameter is the project name
#: adminpages.py:321
#, python-format
msgid "Pootle Admin: %s %s"
msgstr "Administrador do Pootle: %s %s"

#: adminpages.py:322
msgid "Project home page"
msgstr "Página inicial do projecto"

#: adminpages.py:340
#, python-format
msgid "Cannot set rights for username %s - user does not exist"
msgstr ""
"Não pode configurar permissões para o utilizador %s - o utilizador não existe"

#: adminpages.py:357
msgid "This is a GNU-style project (one directory, files named per language)."
msgstr ""
"Este é um projecto ao estilo GNU (uma pasta com ficheiros denominados tendo "
"em conta o idioma)."

#: adminpages.py:359
msgid "This is a standard style project (one directory per language)."
msgstr "Este é um projecto de estilo padrão (uma pasta por idioma)."

#: adminpages.py:360
msgid "User Permissions"
msgstr "Permissões do Utilizador"

#: adminpages.py:361 users.py:98 users.py:134
msgid "Username"
msgstr "Nome de utilizador"

#: adminpages.py:362
msgid "(select to add user)"
msgstr "(seleccionar para adicionar utilizador)"

#: adminpages.py:363
msgid "Rights"
msgstr "Permissões"

#: adminpages.py:364 indexpage.py:998
msgid "Remove"
msgstr "Remover"

#: adminpages.py:391
msgid "Update Rights"
msgstr "Actualizar Permissões"

#: pagelayout.py:37
msgid "All projects"
msgstr "Todos os Projectos"

#: pagelayout.py:38
msgid "All languages"
msgstr "Todos os idiomas"

#: pagelayout.py:39
msgid "My account"
msgstr "A minha conta"

#: pagelayout.py:40 pagelayout.py:168 indexpage.py:200 indexpage.py:262
msgid "Admin"
msgstr "Administrador"

#: pagelayout.py:41
msgid "Docs & help"
msgstr "Docs & ajuda"

#: pagelayout.py:42
msgid "Log out"
msgstr "Sair"

#: pagelayout.py:43
msgid "Log in"
msgstr "Identificação"

#. l10n: Verb, as in "to register"
#: pagelayout.py:45
msgid "Register"
msgstr "Registar"

#: pagelayout.py:46
msgid "Activate"
msgstr "Activar"

#: pagelayout.py:80
msgid "Pootle Logo"
msgstr "Logótipo do Pootle"

#: pagelayout.py:81
msgid "WordForge Translation Project"
msgstr "Projecto de Traduções WordForge"

#: pagelayout.py:83
msgid "About this Pootle server"
msgstr "Sobre este servidor Pootle"

#: pagelayout.py:157
msgid "All goals"
msgstr "Todas as metas"

#: pagelayout.py:228
#, python-format
msgid "%d/%d files"
msgstr "%d/%d ficheiros"

#: pagelayout.py:231
#, python-format
msgid "%d file"
msgid_plural "%d files"
msgstr[0] "ficheiro %d"
msgstr[1] "ficheiros %d"

#: pagelayout.py:232 indexpage.py:995
#, python-format
msgid "%d/%d words (%d%%) translated"
msgstr "%d/%d palavras (%d%%) traduzidas"

#: pagelayout.py:233
#, python-format
msgid "%d/%d strings"
msgstr "%d/%d frases"

#: pagelayout.py:239 users.py:159
msgid "Name"
msgstr "Nome"

#: pagelayout.py:240
msgid "Translated"
msgstr "Traduzido"

#: pagelayout.py:241
msgid "Translated percentage"
msgstr "Percentagem traduzida"

#: pagelayout.py:242
msgid "Translated words"
msgstr "Palavras traduzidas"

#: pagelayout.py:244
msgid "Fuzzy percentage"
msgstr "Percentagem de traduções aproximadas (fuzzys)"

#: pagelayout.py:245
msgid "Fuzzy words"
msgstr "Palavras aproximadas (fuzzy)"

#: pagelayout.py:246 indexpage.py:786
msgid "Untranslated"
msgstr "Por traduzir"

#: pagelayout.py:247
msgid "Untranslated percentage"
msgstr "Percentagem não traduzida"

#: pagelayout.py:248
msgid "Untranslated words"
msgstr "Palavras não traduzidas"

#: pagelayout.py:249
msgid "Total"
msgstr "Total"

#: pagelayout.py:250
msgid "Total words"
msgstr "Total de palavras"

#. l10n: noun. The graphical representation of translation status
#: pagelayout.py:252
msgid "Graph"
msgstr "Gráfico"

#: users.py:39
#, python-format
msgid "You must supply a valid password of at least %d characters."
msgstr "Tem de indicar uma senha válida com pelo menos %d caracteres."

#: users.py:41
msgid "The password is not the same as the confirmation."
msgstr "A senha não coincide com a confirmação"

#: users.py:55
msgid "Login to Pootle"
msgstr "Identificar-se no Pootle"

#: users.py:61
msgid "Username:"
msgstr "Nome de utilizador:"

#: users.py:63
msgid "Password:"
msgstr "Senha:"

#: users.py:64
msgid "Language:"
msgstr "Idioma:"

#: users.py:73
msgid "Default"
msgstr "Por omissão"

#: users.py:89
msgid "Please enter your registration details"
msgstr "Por favor introduza os detalhes do seu registo"

#: users.py:92 users.py:419
msgid "Pootle Registration"
msgstr "Registo no Pootle"

#: users.py:99 users.py:135
msgid "Your requested username"
msgstr "O seu nome de utilizador pretendido"

#: users.py:102 users.py:373
msgid "You must supply a valid email address"
msgstr "Tem de fornecer um endereço de email válido"

#: users.py:105
msgid "Your full name"
msgstr "O seu nome completo"

#: users.py:108
msgid "Your desired password"
msgstr "A sua senha desejada"

#: users.py:110 users.py:164
msgid "Confirm password"
msgstr "Confirme a sua senha"

#: users.py:111
msgid "Type your password again to ensure it is entered correctly"
msgstr "Introduza novamente a sua senha para verificação"

#: users.py:113
msgid "Register Account"
msgstr "Registar conta"

#: users.py:122
msgid "Please enter your activation details"
msgstr "Por favor introduza os detalhes da sua activação"

#: users.py:127
msgid "Pootle Account Activation"
msgstr "Activação da conta Pootle "

#: users.py:137
msgid "Activation Code"
msgstr "Código de activação"

#: users.py:138
msgid "The activation code you received"
msgstr "O código de activação que recebeu"

#: users.py:140
msgid "Activate Account"
msgstr "Activar Conta"

#: users.py:151
#, python-format
msgid "Options for: %s"
msgstr "Opções para: %s"

#: users.py:156
msgid "Personal Details"
msgstr "Detalhes Pessoais"

#: users.py:161
msgid "Email"
msgstr "Email"

#: users.py:165
msgid "Translation Interface Configuration"
msgstr "Configuração da Interface de Tradução"

#: users.py:166
msgid "User Interface language"
msgstr "Idioma do Interface de Utilizador"

#: users.py:167
msgid "My Projects"
msgstr "Meus Projectos"

#: users.py:169
msgid "My Languages"
msgstr "Meus Idiomas"

#: users.py:171
msgid "Home page"
msgstr "Página inicial"

#: users.py:207
msgid "Input Height (in lines)"
msgstr "Altura da zona de escrita (em linhas)"

#: users.py:208
msgid "Number of rows in view mode"
msgstr "Número de linhas no modo de visualização"

#: users.py:209
msgid "Number of rows in translate mode"
msgstr "Número de linhas no modo de tradução"

#: users.py:251
msgid "Error"
msgstr "Erro"

#: users.py:303
msgid "You need to be siteadmin to change users"
msgstr "Você precisa de ser administrador do site para alterar utilizadores"

#: users.py:367
msgid ""
"Username must be alphanumeric, and must start with an alphabetic character."
msgstr "O nome do utilizador é alfanumérico e tem que começar com uma letra."

#: users.py:382
msgid ""
"You (or someone else) attempted to register an account with your username.\n"
msgstr ""
"Você (ou outra pessoa) tentou registar uma conta com o seu nome de "
"utilizador.\n"

#: users.py:383
msgid "We don't store your actual password but only a hash of it.\n"
msgstr "Não guardamos a password mas apenas uma hash da mesma.\n"

#: users.py:385
#, python-format
msgid "If you have a problem with registration, please contact %s.\n"
msgstr "Se tiver algum problema com o registo, por favor contacte %s.\n"

#: users.py:387
msgid ""
"If you have a problem with registration, please contact the site "
"administrator.\n"
msgstr ""
"Se tiver algum problema com o registo, por favor contacte o administrador do "
"site.\n"

#: users.py:388
msgid ""
"That username already exists. An email will be sent to the registered email "
"address.\n"
msgstr ""
"O nome de utilizador já existe. Será enviado um email para o endereço de "
"email registado."

#: users.py:390
#, python-format
msgid "Proceeding to <a href='%s'>login</a>\n"
msgstr "A prosseguir para o <a href='%s'>login</a>\n"

#: users.py:396
msgid "A Pootle account has been created for you using this email address.\n"
msgstr ""
"Foi criada uma conta Pootle para si utilizando este endereço de email.\n"

#: users.py:398
msgid "To activate your account, follow this link:\n"
msgstr "Para activar a sua conta, siga este link:\n"

#: users.py:404
#, python-format
msgid ""
"Your activation code is:\n"
"%s\n"
msgstr "O seu código de activação é:\n"

#: users.py:406
msgid ""
"If you are unable to follow the link, please enter the above code at the "
"activation page.\n"
msgstr ""
"Se não poder seguir o link, por favor introduza o código acima na página de "
"activação.\n"

#: users.py:407
msgid ""
"This message is sent to verify that the email address is in fact correct. If "
"you did not want to register an account, you may simply ignore the message.\n"
msgstr ""
"Esta mensagem é enviada para verificar que o endereço de email é de facto "
"correcto. Se não queria registar uma conta, pode simplesmente ignorar a "
"mensagem.\n"

#: users.py:409
#, python-format
msgid ""
"Account created. You will be emailed login details and an activation code. "
"Please enter your activation code on the <a href='%s'>activation page</a>."
msgstr ""
"Conta criada. Ser-lhe-á enviado por email os detalhes de login e um código "
"de activação. Por favor introduza o seu código de activação na <a href='%"
"s'>página de activação</a>."

#: users.py:411
msgid "(Or simply click on the activation link in the email)"
msgstr "(Ou simplesmente clique no link de activação no email)"

#: users.py:413
#, python-format
msgid "Your user name is: %s\n"
msgstr "O seu nome de utilizador é: %s\n"

#: users.py:415
#, python-format
msgid "Your password is: %s\n"
msgstr "A sua senha é: %s\n"

#: users.py:416
#, python-format
msgid "Your registered email address is: %s\n"
msgstr "O seu endereço de email registado é: %s\n"

#: users.py:442
msgid "Redirecting to Registration Page..."
msgstr "A Redireccionar para a Página de Registo..."

#: users.py:466
msgid "Redirecting to login Page..."
msgstr "A redireccionar para a página de login..."

#: users.py:469
msgid "Your account has been activated! Redirecting to login..."
msgstr "A sua conta foi activada! A redireccionar para o login..."

#: users.py:473
msgid "The activation information was not valid."
msgstr "A informação de activação não era válida."

#: users.py:474
msgid "Activation Failed"
msgstr "A activação Falhou"

#: users.py:583
msgid "Input height must be numeric"
msgstr "A altura tem de ser um número"

#: users.py:584
msgid "Input width must be numeric"
msgstr "A largura tem de ser um número"

#: users.py:585
msgid "The number of rows displayed in view mode must be numeric"
msgstr ""
"O número de linhas mostradas no modo de visualização tem de ser numérico"

#: users.py:586
msgid "The number of rows displayed in translate mode must be numeric"
msgstr "O número de linhas mostradas no modo de tradução tem de ser numérico"

#: pootle.py:293
msgid "Login failed"
msgstr "Login falhou"

#: pootle.py:337 pootle.py:368
msgid "Redirecting to login..."
msgstr "A redireccionar para o login..."

#: pootle.py:340
msgid "Need to log in to access home page"
msgstr "Precisa de fazer login para aceder á home page"

#: pootle.py:353
msgid "Personal details updated"
msgstr "Foram actualizados os seus detalhes pessoais"

#: pootle.py:371
msgid "Need to log in to access admin page"
msgstr "Precisa de fazer login para aceder à página de administração"

#: pootle.py:378
msgid "Redirecting to home..."
msgstr "A redireccionar para a home..."

#: pootle.py:381
msgid "You do not have the rights to administer pootle."
msgstr "Você não tem permissões para gerir o pootle."

#: indexpage.py:61
msgid "About Pootle"
msgstr "Sobre o Pootle"

#. l10n: Take care to use HTML tags correctly. A markup error could cause a display error.
#: indexpage.py:63
msgid ""
"<strong>Pootle</strong> is a simple web portal that should allow you to "
"<strong>translate</strong>! Since Pootle is <strong>Free Software</strong>, "
"you can download it and run your own copy if you like. You can also help "
"participate in the development in many ways (you don't have to be able to "
"program)."
msgstr ""
"<strong>Pootle</strong> é um portal da internet simples que permite a você "
"<strong>traduzir</strong>! Como o Pootle é um <strong>Programa Livre</"
"strong>, você pode fazer download e correr a sua própria cópia se você "
"quiser. Poderá também ajudar participando no seu desenvolvimento de muitas "
"maneiras (você não precisa de saber programar)."

#: indexpage.py:64
msgid ""
"The Pootle project itself is hosted at <a href=\"http://translate."
"sourceforge.net/\">translate.sourceforge.net</a> where you can find the "
"details about source code, mailing lists etc."
msgstr ""
"O projeto Pootle em si está alojado em <a href=\"http://translate."
"sourceforge.net/\">translate.souceforge.net</a> onde poderá encontrar "
"detalhes sobre o código fonte, lista de correspondência etc."

#. l10n: If your language uses right-to-left layout and you leave the English untranslated, consider enclosing the necessary text with <span dir="ltr">.......</span> to help browsers to display it correctly.
#. l10n: Take care to use HTML tags correctly. A markup error could cause a display error.
#: indexpage.py:67
msgid ""
"The name stands for <b>PO</b>-based <b>O</b>nline <b>T</b>ranslation / <b>L</"
"b>ocalization <b>E</b>ngine, but you may need to read <a href=\"http://www."
"thechestnut.com/flumps.htm\">this</a>."
msgstr ""
"O nome significa <b>PO</b>-based <b>O</b>nline <b>T</b>ranslation / <b>L</"
"b>ocalization <b>E</b>ngine, mas poderá precisar de ler <a href=\"http://www."
"thechestnut.com/flumps.htm\">isto</a>."

#: indexpage.py:68
msgid "Versions"
msgstr "Versões"

#. l10n: If your language uses right-to-left layout and you leave the English untranslated, consider enclosing the necessary text with <span dir="ltr">.......</span> to help browsers to display it correctly.
#. l10n: Take care to use HTML tags correctly. A markup error could cause a display error.
#: indexpage.py:71
#, python-format
msgid ""
"This site is running:<br />Pootle %s<br />Translate Toolkit %s<br />jToolkit "
"%s<br />Kid %s<br />ElementTree %s<br />Python %s (on %s/%s)"
msgstr ""
"Este site está a correr:<br />Pootle %s<br />Translate Toolkit %s<br /"
">jToolkit %s<br />Kld %s<br />ElementTree %s<br />Python %s (em %s/%s)"

#: indexpage.py:130
#, python-format
msgid "User Page for: %s"
msgstr "Página do Utilizador para: %s"

#: indexpage.py:132
msgid "Change options"
msgstr "Alterar opções"

#: indexpage.py:133
msgid "Admin page"
msgstr "Página do Administrador"

#: indexpage.py:135
msgid "Quick Links"
msgstr "Links Rápidos"

#: indexpage.py:139
msgid "Please click on 'Change options' and select some languages and projects"
msgstr ""
"Por favor clicar em 'Alterar opções' e selecione alguns idiomas e projetos"

#: indexpage.py:192
#, python-format
msgid "%d project, average %d%% translated"
msgid_plural "%d projects, average %d%% translated"
msgstr[0] "%d Projecto, média de %d%% traduzido"
msgstr[1] "%d Projectos, média de %d%% traduzido"

#. l10n: The first parameter is the name of the installation
#. l10n: The second parameter is the name of the project/language
#. l10n: This is used as a page title. Most languages won't need to change this
#: indexpage.py:198 indexpage.py:260
#, python-format
msgid "%s: %s"
msgstr "%s: %s"

#: indexpage.py:214
msgid "Language Code"
msgstr "Código de Idioma"

#: indexpage.py:215
msgid "Language Name"
msgstr "Nome do Idioma"

#: indexpage.py:252
#, python-format
msgid "%d language, average %d%% translated"
msgid_plural "%d languages, average %d%% translated"
msgstr[0] "Idioma %d, média de %d%% traduzido"
msgstr[1] "Idiomas %d, média de %d%% traduzido"

#: indexpage.py:265
msgid "Language"
msgstr "Idioma"

#. l10n: The first parameter is the name of the installation (like "Pootle")
#: indexpage.py:348
#, python-format
msgid "%s: Project %s, Language %s"
msgstr "%s: Projecto %s, Idioma %s"

#: indexpage.py:413
msgid "Cannot upload file, no file attached"
msgstr "Não pode enviar arquivo, nenhum arquivo anexado"

#: indexpage.py:419
msgid "Can only upload PO files and zips of PO files"
msgstr "Pode-se somente enviar arquivos PO e zips de ficheiros PO"

#: indexpage.py:534
msgid "goals"
msgstr "metas"

#: indexpage.py:535
msgid "Enter goal name"
msgstr "Introduza o nome da meta"

#: indexpage.py:536
msgid "Add Goal"
msgstr "Adicionar meta"

#: indexpage.py:541 indexpage.py:543
msgid "Upload File"
msgstr "Enviar Ficheiro"

#: indexpage.py:542
msgid "Select file to upload"
msgstr "Selecione o ficheiro para enviar"

#. l10n: tooltip
#: indexpage.py:550
msgid "Overwrite the current file if it exists"
msgstr "Sobreescrever o ficheiro actual se este existir"

#. l10n: radio button text
#: indexpage.py:552
msgid "Merge"
msgstr "Juntar"

#. l10n: tooltip
#: indexpage.py:554
msgid ""
"Merge the file with the current file and turn conflicts into suggestions"
msgstr ""
"Juntar o ficheiro com o ficheiro actual e tornar os conflitos em sugestões"

#: indexpage.py:657
msgid "Not in a goal"
msgstr "Não numa meta"

#: indexpage.py:677
msgid "Add User"
msgstr "Adicionar utilizador"

#: indexpage.py:719
msgid "PO file"
msgstr "Ficheiro PO"

#: indexpage.py:723
msgid "XLIFF file"
msgstr "Ficheiro XLIFF"

#: indexpage.py:727
msgid "Qt .ts file"
msgstr "Ficheiro QT .ts "

#: indexpage.py:731
msgid "CSV file"
msgstr "Ficheiro CSV"

#: indexpage.py:736
msgid "MO file"
msgstr "Ficheiro MO"

#. l10n: Update from version control (like CVS or Subversion)
#: indexpage.py:742
msgid "Update"
msgstr "Actualizar"

#: indexpage.py:785
msgid "All Strings"
msgstr "Todas as Frases"

#: indexpage.py:787
msgid "Unassigned"
msgstr "Sem Designação"

#: indexpage.py:788
msgid "Unassigned and Untranslated"
msgstr "Sem Designação e Tradução"

#: indexpage.py:795
msgid "Set Goal"
msgstr "Configurar Meta"

#: indexpage.py:799
msgid "Select Multiple"
msgstr "Escolha Múltipla"

#: indexpage.py:801
msgid "Assign To"
msgstr "Designar a"

#: indexpage.py:830
msgid "Show Editing Functions"
msgstr "Mostrar Funções de Edição"

#: indexpage.py:831
msgid "Show Statistics"
msgstr "Mostrar Estatísticas"

#: indexpage.py:832
msgid "Show Tracks"
msgstr "Mostrar Caminhos"

#: indexpage.py:832
msgid "Hide Tracks"
msgstr "Esconder Caminhos"

#. l10n: "Checks" are quality checks that Pootle performs on translations to test for common mistakes
#: indexpage.py:834
msgid "Show Checks"
msgstr "Mostrar Verificações"

#: indexpage.py:834
msgid "Hide Checks"
msgstr "Esconder Verificações"

#: indexpage.py:835
msgid "Show Goals"
msgstr "Mostrar Metas"

#: indexpage.py:835
msgid "Hide Goals"
msgstr "Esconder Metas"

#: indexpage.py:836
msgid "Show Assigns"
msgstr "Mostrar Designações"

#: indexpage.py:836
msgid "Hide Assigns"
msgstr "Esconder Designações"

#: indexpage.py:845
#, python-format
msgid "All Goals: %s"
msgstr "Todas as Metas: %s"

#: indexpage.py:851
msgid "Translate My Strings"
msgstr "Traduzir as Minhas Frases"

#: indexpage.py:853
msgid "View My Strings"
msgstr "Ver as Minhas Frases"

#: indexpage.py:858
msgid "No strings assigned to you"
msgstr "Nenhuma frase lhe foi designada"

#: indexpage.py:862
msgid "Quick Translate My Strings"
msgstr "Traduzir Rápido as Minhas Frases"

#: indexpage.py:866
msgid "No untranslated strings assigned to you"
msgstr "Nenhuma frase sem tradução lhe foi designada"

#: indexpage.py:870
msgid "Review Suggestions"
msgstr "Rever Sugestões"

#: indexpage.py:872
msgid "View Suggestions"
msgstr "Ver Sugestões"

#: indexpage.py:877
msgid "Quick Translate"
msgstr "Tradução Rápida"

#: indexpage.py:879
msgid "View Untranslated"
msgstr "Ver Não Traduzidas"

#: indexpage.py:883
msgid "No untranslated items"
msgstr "Nenhum item sem tradução"

#: indexpage.py:886
msgid "Translate All"
msgstr "Traduzir tudo"

#: indexpage.py:901
msgid "ZIP of goal"
msgstr "ZIP da meta"

#: indexpage.py:903
msgid "ZIP of folder"
msgstr "ZIP do directório"

#: indexpage.py:910
msgid "Generate SDF"
msgstr "Gerar SDF"

#: indexpage.py:963
#, python-format
msgid "%d string (%d%%) failed"
msgid_plural "%d strings (%d%%) failed"
msgstr[0] "%d frase (%d%%) falhou"
msgstr[1] "%d frases (%d%%) falharam"

#: indexpage.py:993
#, python-format
msgid "%d/%d words (%d%%) assigned"
msgstr "%d/%d palavras (%d%%) designadas"

#: indexpage.py:994 indexpage.py:996
#, python-format
msgid "[%d/%d strings]"
msgstr "[%d/%d frases]"

#~ msgid "Broaden"
#~ msgstr "Alargar"

#~ msgid "Narrow"
#~ msgstr "Estreitar"

#~ msgid "Reset"
#~ msgstr "Restaurar"

#~ msgid "Must be a valid email address"
#~ msgstr "Tem de ser um endereço de email válido"

#~ msgid "Input Width (in characters)"
#~ msgstr "Largura da zona de escrita (em caracteres)"

#~ msgid "Pootle"
#~ msgstr "Pootle"

#~ msgid "Pootle: %s"
#~ msgstr "Pootle: %s"

#~ msgid ""
#~ "<strong>Pootle</strong> is a simple web portal that should allow you to "
#~ "<strong>translate</strong>!"
#~ msgstr ""
#~ "<strong>Pootle</strong> é um simples portal internet que lhe "
#~ "permite<strong>traduzir</strong>"

#~ msgid "parent folder"
#~ msgstr "pasta de origem"

#~ msgid "<h3 class=\"title\">Projects</h3>"
#~ msgstr "<h3 class=\"title\">Projetos</h3>"

#~ msgid "%d files, %d/%d strings (%d%%) translated"
#~ msgstr "%d arquivos, %d/%d frases (%d%%) traduzidos"

#~ msgid "%s language"
#~ msgstr "idioma %s "

#~ msgid "%d strings (%d%%) failed"
#~ msgstr "%d frases (%d%%) falharam"

#~ msgid "this is a demo installation of pootle"
#~ msgstr "esta é uma instalação demo do pootle"

#~ msgid "current folder"
#~ msgstr "diretório atual"

#~ msgid "Log In"
#~ msgstr "Identificação"

#~ msgid "login status"
#~ msgstr "status do login"

#~ msgid "project root"
#~ msgstr "Raíz do projeto"

#~ msgid "%s project"
#~ msgstr "projecto %s "

#~ msgid "you do not have rights to suggest changes here"
#~ msgstr "você não tem o direito de sugerir mudanças aqui"

#~ msgid "you do not have rights to change translations here"
#~ msgstr "você não tem o direito de alterar traduções aqui"

#~ msgid "%d/%d strings (%d%%) translated"
#~ msgstr "%d/%d frases(%d%%) traduzidas"

#~ msgid "%d files, "
#~ msgstr "%d arquivos,"

#~ msgid "<h3 class=\"title\">Languages</h3>"
#~ msgstr "<h3 class=\"title\">Idiomas</h3>"

#~ msgid "current file"
#~ msgstr "arquivo atual"

#~ msgid "not logged in"
#~ msgstr "não identificado"

#~ msgid "Pootle Admin Page"
#~ msgstr "Página do Administrador do Pootle"

#~ msgid "Pootle Languages Admin Page"
#~ msgstr "Página de idiomas do Administrador do Pootle"

#~ msgid "Pootle Projects Admin Page"
#~ msgstr "Página de Projetos do Administrador do Pootle"

#~ msgid "You do not have the rights to administer Pootle."
#~ msgstr "Você não tem o direito de administrar o Pootle."

#~ msgid "Pootle User Admin Page"
#~ msgstr "Página de Usuário do Administrador do Pootle"

#~ msgid "suggest"
#~ msgstr "sugerir"

#~ msgid "Input Width"
#~ msgstr "Digite a Largura"
