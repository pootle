msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: translate-pootle@lists.sourceforge.net\n"
"POT-Creation-Date: 2007-04-13 12:24+0200\n"
"PO-Revision-Date: 2008-03-18 21:25+0000\n"
"Last-Translator: Miklos Vajna <vmiklos@frugalware.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 1.1.0rc2\n"
"Generated-By: pygettext.py 1.1\n"

#. l10n: Verb
#: projects.py:136
msgid "View"
msgstr "Nézet"

#: projects.py:137 translatepage.py:613
msgid "Suggest"
msgstr "Javaslat"

#: projects.py:138
msgid "Translate"
msgstr "Fordítás"

#. l10n: radio button text
#: projects.py:139 indexpage.py:548
msgid "Overwrite"
msgstr "Felülírás"

#. l10n: Verb
#: projects.py:141
msgid "Review"
msgstr "Áttekintés"

#. l10n: Verb
#: projects.py:143
msgid "Archive"
msgstr "Arhívum"

#. l10n: This refers to generating the binary .mo file
#: projects.py:145
msgid "Compile PO files"
msgstr "PO fájlok szerkesztése"

#: projects.py:146
msgid "Assign"
msgstr "Hozzárendelés"

#: projects.py:147 indexpage.py:134
msgid "Administrate"
msgstr "Adminisztráció"

#. l10n: Commit to version control (like CVS or Subversion)
#: projects.py:148 indexpage.py:748
msgid "Commit"
msgstr "Beküldés"

#. l10n: Don't translate "nobody" or "default"
#: projects.py:228
msgid "You cannot remove the \"nobody\" or \"default\" user"
msgstr "Ön nem tudja eltávolítani a \"nobody\" vagy \"default\" felhasználót"

#: projects.py:380 projects.py:442
msgid "You do not have rights to alter goals here"
msgstr "Ön itt nem rendelkezik jogokkal a cél módosításához"

#: projects.py:505
msgid "You do not have rights to upload files here"
msgstr "Ön itt nem rendelkezik jogokkal fájl feltöltéshez"

#: projects.py:508
msgid "You do not have rights to overwrite files here"
msgstr "Ön itt nem rendelkezik jogokkal fájl felülírásához"

#: projects.py:510
msgid "You do not have rights to upload new files here"
msgstr "Ön itt nem rendelkezik jogokkal új fájl feltöltéshez"

#: projects.py:519
msgid "You do not have rights to update files here"
msgstr "Ön itt nem rendelkezik jogokkal fájl aktualizáláshoz"

#: projects.py:590
msgid "You do not have rights to commit files here"
msgstr "Ön itt nem rendelkezik jogokkal fájl beküldéséhez"

#: projects.py:919 projects.py:960
msgid "You do not have rights to alter assignments here"
msgstr "Ön itt nem rendelkezik jogokkal kijelölt feladat megváltoztatásához"

#: projects.py:1141
msgid "You do not have rights to change translations here"
msgstr "Ön itt nem rendelkezik jogokkal fordítás megváltoztatásához"

#: projects.py:1151
msgid "You do not have rights to suggest changes here"
msgstr "Ön itt nem rendelkezik jogokkal javaslat megváltoztatásához"

#: projects.py:1167 projects.py:1185
msgid "You do not have rights to review suggestions here"
msgstr "Ön itt nem rendelkezik jogokkal javaslatok bírálatához"

#: translatepage.py:89
#, python-format
msgid ""
"%d/%d translated\n"
"(%d blank, %d fuzzy)"
msgstr ""
"%d/%d lefordítva\n"
"(%d üres, %d bizonytalan)"

#: translatepage.py:94 adminpages.py:35 adminpages.py:74 adminpages.py:132
#: adminpages.py:197 adminpages.py:284 adminpages.py:344 pagelayout.py:72
#: users.py:58 users.py:95 users.py:131 users.py:153 indexpage.py:73
#: indexpage.py:96 indexpage.py:136 indexpage.py:194 indexpage.py:256
#: indexpage.py:346
msgid "Pootle Demo"
msgstr "Pootle Bemutató"

#. l10n: first parameter: name of the installation (like "Pootle")
#. l10n: second parameter: project name
#. l10n: third parameter: target language
#. l10n: fourth parameter: file name
#: translatepage.py:99
#, python-format
msgid "%s: translating %s into %s: %s"
msgstr "%s: %s fordítása %s-ba: %s"

#. l10n: Heading above the table column with the source language
#: translatepage.py:114
msgid "Original"
msgstr "Eredeti"

#. l10n: Heading above the table column with the target language
#: translatepage.py:116
msgid "Translation"
msgstr "Mindet lefordít"

#: translatepage.py:119
msgid "Accept"
msgstr "Elfogad"

#: translatepage.py:120
msgid "Reject"
msgstr "Elvet"

#: translatepage.py:121 pagelayout.py:243
msgid "Fuzzy"
msgstr "Bizonytalan"

#. l10n: Heading above the textarea for translator comments.
#: translatepage.py:123
msgid "Translator comments"
msgstr "Fordítói megjegyzések"

#. l10n: Heading above the comments extracted from the programing source code
#: translatepage.py:125
msgid "Developer comments"
msgstr "Fejlesztői megjegyzések"

#. l10n: This heading refers to related translations and terminology
#: translatepage.py:127
msgid "Related"
msgstr "Hasonló"

#. l10n: text next to search field
#: translatepage.py:131 indexpage.py:356
msgid "Search"
msgstr "Keresés"

#. l10n: "batch" refers to the set of translations that were reviewed
#: translatepage.py:145
msgid "End of batch"
msgstr "Köteg vége"

#: translatepage.py:147
msgid "Click here to return to the index"
msgstr "Visszatérés a tartalomjegyzékhez"

#. l10n: noun (the start)
#: translatepage.py:161 translatepage.py:164
msgid "Start"
msgstr "Kezd"

#. l10n: the parameter refers to the number of messages
#: translatepage.py:168 translatepage.py:171
#, python-format
msgid "Previous %d"
msgstr "Előző %d"

#. l10n: the third parameter refers to the total number of messages in the file
#: translatepage.py:173
#, python-format
msgid "Items %d to %d of %d"
msgstr "%d-%d elem a %d-ból/ből"

#. l10n: the parameter refers to the number of messages
#: translatepage.py:178 translatepage.py:181
#, python-format
msgid "Next %d"
msgstr "Következő %d"

#. l10n: noun (the end)
#: translatepage.py:184 translatepage.py:187
msgid "End"
msgstr "Vége"

#. l10n: the parameter is the name of one of the quality checks, like "fuzzy"
#: translatepage.py:204
#, python-format
msgid "checking %s"
msgstr "%s ellenőrzése"

#: translatepage.py:211 translatepage.py:214 indexpage.py:526 indexpage.py:529
msgid "Assign Strings"
msgstr "Kiosztott mondatok"

#: translatepage.py:212 indexpage.py:528
msgid "Assign to User"
msgstr "Felhasználónak kiosztva"

#: translatepage.py:213 indexpage.py:527
msgid "Assign Action"
msgstr "Kiosztott feladatok"

#: translatepage.py:368
#, python-format
msgid "There are no items matching that search ('%s')"
msgstr "Nincs a ('%s') feltételnek megfelelő találat"

#: translatepage.py:370
msgid "You have finished going through the items you selected"
msgstr "Végigért a kiválasztott elemeken"

#: translatepage.py:578
msgid "Singular"
msgstr "Páratlan"

#: translatepage.py:579
msgid "Plural"
msgstr "Többszörös"

#. l10n: verb
#: translatepage.py:593
msgid "Edit"
msgstr "Szerkeszt"

#. l10n: verb
#: translatepage.py:609
msgid "Copy"
msgstr "Másol"

#: translatepage.py:610 translatepage.py:784
msgid "Skip"
msgstr "Átugrás"

#. l10n: verb
#: translatepage.py:612 translatepage.py:783 users.py:256
msgid "Back"
msgstr "Visszamegy"

#: translatepage.py:614
msgid "Submit"
msgstr "Állít"

#. l10n: action that increases the height of the textarea
#: translatepage.py:617
msgid "Grow"
msgstr "Növel"

#. l10n: action that decreases the height of the textarea
#: translatepage.py:619
msgid "Shrink"
msgstr "Zsugorít"

#: translatepage.py:641 translatepage.py:735 translatepage.py:772
#: translatepage.py:805
#, python-format
msgid "Plural Form %d"
msgstr "Többszörös forma %d"

#. l10n: This is an error message that will display if the relevant problem occurs
#: translatepage.py:657
msgid ""
"Translation not possible because plural information for your language is not "
"available. Please contact the site administrator."
msgstr ""
"Fordítás nem lehetséges, mert az Ön nyelvében nincsenek többesszámok. Kérem, "
"lépjen kapcsolatba az oldal üzemeltetőjével."

#: translatepage.py:739
msgid "Current Translation:"
msgstr "Jelenlegi fordítás:"

#. l10n: First parameter: number
#. l10n: Second parameter: name of translator
#: translatepage.py:752
#, python-format
msgid "Suggestion %d by %s:"
msgstr "%d. javaslat %s által:"

#: translatepage.py:754
#, python-format
msgid "Suggestion %d:"
msgstr "%d. javaslat:"

#. l10n: parameter: name of translator
#: translatepage.py:758
#, python-format
msgid "Suggestion by %s:"
msgstr "%s javaslata:"

#: translatepage.py:760
msgid "Suggestion:"
msgstr "Javaslat:"

#: adminpages.py:43 adminpages.py:82 adminpages.py:140 adminpages.py:205
#: pagelayout.py:36
msgid "Home"
msgstr "Kezdőlap"

#: adminpages.py:44 adminpages.py:207
msgid "Users"
msgstr "Felhasználók"

#: adminpages.py:45 adminpages.py:84 indexpage.py:94
msgid "Languages"
msgstr "Nyelvek"

#: adminpages.py:46 adminpages.py:142 indexpage.py:95
msgid "Projects"
msgstr "Projektek"

#: adminpages.py:47
msgid "General options"
msgstr "Általános opciók"

#: adminpages.py:48 users.py:157
msgid "Option"
msgstr "Opció"

#: adminpages.py:49 users.py:158
msgid "Current value"
msgstr "Érvényes érték"

#: adminpages.py:50 adminpages.py:85 adminpages.py:143 adminpages.py:208
#: users.py:172
msgid "Save changes"
msgstr "Változások mentése"

#: adminpages.py:54
msgid "Title"
msgstr "Cím"

#: adminpages.py:55
msgid "Description"
msgstr "Megjegyzés"

#: adminpages.py:56
msgid "Base URL"
msgstr "Alap URL"

#: adminpages.py:57
msgid "Home Page"
msgstr "Honlap"

#: adminpages.py:83 adminpages.py:141 adminpages.py:206
msgid "Main admin page"
msgstr "Adminisztráció"

#: adminpages.py:89
msgid "ISO Code"
msgstr "ISO kód"

#: adminpages.py:90 adminpages.py:148 adminpages.py:213 users.py:104
msgid "Full Name"
msgstr "Teljes név"

#: adminpages.py:91
msgid "(add language here)"
msgstr "(nyelv hozzáadása)"

#: adminpages.py:92
msgid "Special Chars"
msgstr "Egyedi jelek"

#: adminpages.py:92
msgid "(special characters)"
msgstr "(egyedi jelek, karakterek)"

#: adminpages.py:93 indexpage.py:217
msgid "Number of Plurals"
msgstr "Többesszám száma"

#: adminpages.py:93
msgid "(number of plurals)"
msgstr "(többes számúak száma)"

#: adminpages.py:94 indexpage.py:218
msgid "Plural Equation"
msgstr "Többszörös kiegyenlítés"

#: adminpages.py:94
msgid "(plural equation)"
msgstr "(többszörös kiegyenlítés)"

#: adminpages.py:95
msgid "Remove Language"
msgstr "Nyelv eltávolítása"

#. l10n: The parameter is a languagecode, projectcode or username
#: adminpages.py:110 adminpages.py:177 adminpages.py:240 adminpages.py:410
#, python-format
msgid "Remove %s"
msgstr "%s eltávolítása"

#: adminpages.py:129
msgid "Standard"
msgstr "Szabvány"

#: adminpages.py:147
msgid "Project Code"
msgstr "Projekt kód"

#: adminpages.py:149
msgid "(add project here)"
msgstr "(projekt hozzáadása)"

#: adminpages.py:150
msgid "Project Description"
msgstr "Projekt megjegyzés"

#: adminpages.py:150
msgid "(project description)"
msgstr "(projekt megjegyzés)"

#: adminpages.py:151
msgid "Checker Style"
msgstr "Ellenőrzés stílusa"

#: adminpages.py:152
msgid "File Type"
msgstr "Fájl típusa"

#: adminpages.py:153
msgid "Create MO Files"
msgstr "MO fájl létrehozása"

#: adminpages.py:154
msgid "Remove Project"
msgstr "Projekt eltávolítása"

#: adminpages.py:212 users.py:66
msgid "Login"
msgstr "Bejelentkezés"

#: adminpages.py:214 users.py:347
msgid "(add full name here)"
msgstr "(teljes név hozzáadása)"

#: adminpages.py:215 users.py:101
msgid "Email Address"
msgstr "E-mail cím"

#: adminpages.py:215 users.py:350
msgid "(add email here)"
msgstr "(e-mail cím hozzáadása)"

#: adminpages.py:216 users.py:107 users.py:163
msgid "Password"
msgstr "Jelszó"

#: adminpages.py:216 users.py:344
msgid "(add password here)"
msgstr "(jelszó hozzáadása)"

#: adminpages.py:217
msgid "Activated"
msgstr "Aktiválva"

#: adminpages.py:217
msgid "Activate New User"
msgstr "Új felhasználó aktiválása"

#: adminpages.py:218
msgid "Remove User"
msgstr "Felhasználó eltávolítása"

#: adminpages.py:272
msgid "Back to main page"
msgstr "Vissza a főoldalra"

#: adminpages.py:273
msgid "Existing languages"
msgstr "Létező nyelvek"

#. l10n: This refers to updating the translation files from the templates like with .pot files
#: adminpages.py:277
msgid "Update Languages"
msgstr "Nyelvek aktualizálása"

#: adminpages.py:278
#, python-format
msgid "Pootle Admin: %s"
msgstr "Pootle adminisztrátor: %s"

#: adminpages.py:279 adminpages.py:341
msgid "You do not have the rights to administer this project."
msgstr "Önnek nincs joga e projektet adminisztrálni."

#. l10n: This refers to updating the translation files from the templates like with .pot files
#: adminpages.py:281
msgid "Update from templates"
msgstr "Aktualizálás sablonból"

#: adminpages.py:289
msgid "Add Language"
msgstr "Nyelv hozzáadása"

#. l10n: This is the page title. The first parameter is the language name, the second parameter is the project name
#: adminpages.py:321
#, python-format
msgid "Pootle Admin: %s %s"
msgstr "Pootle adminisztrátor: %s %s"

#: adminpages.py:322
msgid "Project home page"
msgstr "Projekt honlap"

#: adminpages.py:340
#, python-format
msgid "Cannot set rights for username %s - user does not exist"
msgstr "Nem tudom beállítani %s jogait - felhasználó nem létezik"

#: adminpages.py:357
msgid "This is a GNU-style project (one directory, files named per language)."
msgstr ""
"Ez egy GNU stílusú projekt (egy könyvtár, fájlok elnevezve nyelvenként)."

#: adminpages.py:359
msgid "This is a standard style project (one directory per language)."
msgstr "Ez egy sztenderd stílusú projekt (egy könyvtár nyelvenként)."

#: adminpages.py:360
msgid "User Permissions"
msgstr "Felhasználói engedélyek"

#: adminpages.py:361 users.py:98 users.py:134
msgid "Username"
msgstr "Felhasználónév"

#: adminpages.py:362
msgid "(select to add user)"
msgstr "(kiválasztott felhasználó hozzáadása)"

#: adminpages.py:363
msgid "Rights"
msgstr "Jogok"

#: adminpages.py:364 indexpage.py:998
msgid "Remove"
msgstr "Eltávolít"

#: adminpages.py:391
msgid "Update Rights"
msgstr "Jogok aktualizálása"

#: pagelayout.py:37
msgid "All projects"
msgstr "Minden projekt"

#: pagelayout.py:38
msgid "All languages"
msgstr "Minden nyelv"

#: pagelayout.py:39
msgid "My account"
msgstr "Saját adatok"

#: pagelayout.py:40 pagelayout.py:168 indexpage.py:200 indexpage.py:262
msgid "Admin"
msgstr "Adminisztáció"

#: pagelayout.py:41
msgid "Docs & help"
msgstr "Doksi és súgó"

#: pagelayout.py:42
msgid "Log out"
msgstr "Kijelentkezés"

#: pagelayout.py:43
msgid "Log in"
msgstr "Bejelentkezés"

#. l10n: Verb, as in "to register"
#: pagelayout.py:45
msgid "Register"
msgstr "Regisztráció"

#: pagelayout.py:46
msgid "Activate"
msgstr "Aktivál"

#: pagelayout.py:80
msgid "Pootle Logo"
msgstr "Pootle Logó"

#: pagelayout.py:81
msgid "WordForge Translation Project"
msgstr "WordForge Fordítási Projekt"

#: pagelayout.py:83
msgid "About this Pootle server"
msgstr "Erről a Pootle szerverről"

#: pagelayout.py:157
msgid "All goals"
msgstr "Minden cél"

#: pagelayout.py:228
#, python-format
msgid "%d/%d files"
msgstr "%d/%d fájl"

#: pagelayout.py:231
#, python-format
msgid "%d file"
msgid_plural "%d files"
msgstr[0] "%d fájl"
msgstr[1] "%d fájl"

#: pagelayout.py:232 indexpage.py:995
#, python-format
msgid "%d/%d words (%d%%) translated"
msgstr "%d/%d szó (%d%%) lefordítva"

#: pagelayout.py:233
#, python-format
msgid "%d/%d strings"
msgstr "%d/%d mondat"

#: pagelayout.py:239 users.py:159
msgid "Name"
msgstr "Név"

#: pagelayout.py:240
msgid "Translated"
msgstr "Lefordítva"

#: pagelayout.py:241
msgid "Translated percentage"
msgstr "Elkészült fordítás"

#: pagelayout.py:242
msgid "Translated words"
msgstr "Lefordított szavak"

#: pagelayout.py:244
msgid "Fuzzy percentage"
msgstr "Bizonytalan fordítás"

#: pagelayout.py:245
msgid "Fuzzy words"
msgstr "Bizonytalan szavak"

#: pagelayout.py:246 indexpage.py:786
msgid "Untranslated"
msgstr "Fordítatlan"

#: pagelayout.py:247
msgid "Untranslated percentage"
msgstr "Fordítatlan"

#: pagelayout.py:248
msgid "Untranslated words"
msgstr "Fordítatlan szavak"

#: pagelayout.py:249
msgid "Total"
msgstr "Összes"

#: pagelayout.py:250
msgid "Total words"
msgstr "Összes szó"

#. l10n: noun. The graphical representation of translation status
#: pagelayout.py:252
msgid "Graph"
msgstr "Grafikon"

#: users.py:39
#, python-format
msgid "You must supply a valid password of at least %d characters."
msgstr "Legalább %d karakter hosszúságú érvényes jelszót kell megadnia."

#: users.py:41
msgid "The password is not the same as the confirmation."
msgstr "A jelszavak nem egyeznek."

#: users.py:55
msgid "Login to Pootle"
msgstr "Pootle bejelentkezés"

#: users.py:61
msgid "Username:"
msgstr "Felhasználónév:"

#: users.py:63
msgid "Password:"
msgstr "Jelszó:"

#: users.py:64
msgid "Language:"
msgstr "Nyelv:"

#: users.py:73
msgid "Default"
msgstr "Alapértelmezés"

#: users.py:89
msgid "Please enter your registration details"
msgstr "Adja meg regisztrációs adatait"

#: users.py:92 users.py:419
msgid "Pootle Registration"
msgstr "Pootle regisztráció"

#: users.py:99 users.py:135
msgid "Your requested username"
msgstr "A kért felhasználónév"

#: users.py:102 users.py:373
msgid "You must supply a valid email address"
msgstr "Valós e-mail cím szükséges"

#: users.py:105
msgid "Your full name"
msgstr "Az Ön teljes neve"

#: users.py:108
msgid "Your desired password"
msgstr "Kért jelszó"

#: users.py:110 users.py:164
msgid "Confirm password"
msgstr "Jelszó megerősítése"

#: users.py:111
msgid "Type your password again to ensure it is entered correctly"
msgstr "Ellenőrzésképpen írja be újra a jelszavát"

#: users.py:113
msgid "Register Account"
msgstr "Hozzáférés regisztrálása"

#: users.py:122
msgid "Please enter your activation details"
msgstr "Adja meg aktiválási adatait"

#: users.py:127
msgid "Pootle Account Activation"
msgstr "Pootle hozzáférés aktiválása"

#: users.py:137
msgid "Activation Code"
msgstr "Aktiválási kód"

#: users.py:138
msgid "The activation code you received"
msgstr "A kapott aktiválási kód"

#: users.py:140
msgid "Activate Account"
msgstr "Hozzáférés aktiválása"

#: users.py:151
#, python-format
msgid "Options for: %s"
msgstr "%s opciói"

#: users.py:156
msgid "Personal Details"
msgstr "Személyes részletek"

#: users.py:161
msgid "Email"
msgstr "E-mail"

#: users.py:165
msgid "Translation Interface Configuration"
msgstr "Fordító felület konfiguráció"

#: users.py:166
msgid "User Interface language"
msgstr "Felhasználói felület nyelve"

#: users.py:167
msgid "My Projects"
msgstr "Saját projektek"

#: users.py:169
msgid "My Languages"
msgstr "Saját nyelvek"

#: users.py:171
msgid "Home page"
msgstr "Honlap"

#: users.py:207
msgid "Input Height (in lines)"
msgstr "Beviteli magasság (sorokban)"

#: users.py:208
msgid "Number of rows in view mode"
msgstr "Sorok száma áttekintés módban"

#: users.py:209
msgid "Number of rows in translate mode"
msgstr "Sorok száma fordítás módban"

#: users.py:251
msgid "Error"
msgstr "Hiba"

#: users.py:303
msgid "You need to be siteadmin to change users"
msgstr "Önnek szüksége lesz a rendszergazdára a felhasználó váltáshoz"

#: users.py:367
msgid ""
"Username must be alphanumeric, and must start with an alphabetic character."
msgstr ""
"A felhasználónév számot és betűt tartalmazhat, és betűvel kell kezdődnie."

#: users.py:382
msgid ""
"You (or someone else) attempted to register an account with your username.\n"
msgstr "Ön (vagy valaki más) megkisérelt regisztrálni az ön felhasználónevével.\n"

#: users.py:383
msgid "We don't store your actual password but only a hash of it.\n"
msgstr "Nem tároljuk el az ön jelenlegi jelszavát csak annak egy ellenőrzőösszegét.\n"

#: users.py:385
#, python-format
msgid "If you have a problem with registration, please contact %s.\n"
msgstr "Ha gondja van a regisztrációval, itt kérhet segítséget: %s.\n"

#: users.py:387
msgid ""
"If you have a problem with registration, please contact the site "
"administrator.\n"
msgstr "Ha gondja van a regisztrációval, kérem lépjen kapcsolatba az oldal "
"üzemeltetőjével.\n"

#: users.py:388
msgid ""
"That username already exists. An email will be sent to the registered email "
"address.\n"
msgstr "Ez a felhasználónév már létezik. Egy email-t küldünk a regisztrált címre.\n"

#: users.py:390
#, python-format
msgid "Proceeding to <a href='%s'>login</a>\n"
msgstr "A <a href='%s'>bejelentkezés</a> folyamatban\n"

#: users.py:396
msgid "A Pootle account has been created for you using this email address.\n"
msgstr "Létrejött a Pootle hozzáférés ezzel az email címmel.\n"

#: users.py:398
msgid "To activate your account, follow this link:\n"
msgstr "A hozzáférés aktiválásához kattintson erre a linkre:\n"

#: users.py:404
#, python-format
msgid ""
"Your activation code is:\n"
"%s\n"
msgstr ""
"Aktiválási kód:\n"
"%s\n"

#: users.py:406
msgid ""
"If you are unable to follow the link, please enter the above code at the "
"activation page.\n"
msgstr "Ha nem működik a link, kérjük írja be az alábbi kódot az aktiváló oldalon.\n"

#: users.py:407
msgid ""
"This message is sent to verify that the email address is in fact correct. If "
"you did not want to register an account, you may simply ignore the message.\n"
msgstr "Ezt az üzenetet az email-cím ellenőrzése céljából küldtük. Ha nem akar "
"regisztrálni, törölheti ezt a levelet.\n"

#: users.py:409
#, python-format
msgid ""
"Account created. You will be emailed login details and an activation code. "
"Please enter your activation code on the <a href='%s'>activation page</a>."
msgstr ""
"A hozzáférés létrejött. Hamarosan email-t kap a bejelentkezési adatokkal, és "
"az aktiváló kóddal. Kérjük írja be az aktiváló kódját az <a "
"href='%s'>aktiváló oldalon</a>."

#: users.py:411
msgid "(Or simply click on the activation link in the email)"
msgstr "(Vagy kattintson az aktiváló linkre a levélben)"

#: users.py:413
#, python-format
msgid "Your user name is: %s\n"
msgstr "Az Ön teljes neve: %s\n"

#: users.py:415
#, python-format
msgid "Your password is: %s\n"
msgstr "Az ön jelszava: %s\n"

#: users.py:416
#, python-format
msgid "Your registered email address is: %s\n"
msgstr "Regisztrált email-címe: %s\n"

#: users.py:442
msgid "Redirecting to Registration Page..."
msgstr "Átirányítás a regisztrációs lapra..."

#: users.py:466
msgid "Redirecting to login Page..."
msgstr "Átirányítás a bejelentkező lapra..."

#: users.py:469
msgid "Your account has been activated! Redirecting to login..."
msgstr "Az Ön hozzáférése aktiválva! Átirányítás bejelentkezéshez..."

#: users.py:473
msgid "The activation information was not valid."
msgstr "Az aktiválás adatai nem érvényesek."

#: users.py:474
msgid "Activation Failed"
msgstr "Aktiválás sikertelen"

#: users.py:583
msgid "Input height must be numeric"
msgstr "Beviteli magasságot számokkal kell megadni"

#: users.py:584
msgid "Input width must be numeric"
msgstr "Beviteli szélességet számokkal kell megadni"

#: users.py:585
msgid "The number of rows displayed in view mode must be numeric"
msgstr "A sorok számát az áttekintés módhoz számokkal kell megadni"

#: users.py:586
msgid "The number of rows displayed in translate mode must be numeric"
msgstr "A sorok számát a fordító módhoz számokkal kell megadni"

#: pootle.py:293
msgid "Login failed"
msgstr "Sikertelen bejelentkezés"

#: pootle.py:337 pootle.py:368
msgid "Redirecting to login..."
msgstr "Átirányítás a bejelentkezéshez..."

#: pootle.py:340
msgid "Need to log in to access home page"
msgstr "A honlap hozzáfáféréséhez szükség van bejelentkezésre"

#: pootle.py:353
msgid "Personal details updated"
msgstr "Személyes adatok fríssitve"

#: pootle.py:371
msgid "Need to log in to access admin page"
msgstr "Az adminisztrációs lap hozzáfáféréséhez szükség van bejelentkezésre"

#: pootle.py:378
msgid "Redirecting to home..."
msgstr "Átirányítás a kezdőlapra..."

#: pootle.py:381
msgid "You do not have the rights to administer pootle."
msgstr "Önnek nincs joga adminisztrálni a pootle-t."

#: indexpage.py:61
msgid "About Pootle"
msgstr "Pootle névjegye"

#. l10n: Take care to use HTML tags correctly. A markup error could cause a display error.
#: indexpage.py:63
msgid ""
"<strong>Pootle</strong> is a simple web portal that should allow you to "
"<strong>translate</strong>! Since Pootle is <strong>Free Software</strong>, "
"you can download it and run your own copy if you like. You can also help "
"participate in the development in many ways (you don't have to be able to "
"program)."
msgstr ""
"<strong>Pootle</strong> egy egyszerű WebPortál, ami hozzájárul az Ön "
"<strong>fordításaihoz</strong>! Mivel a Pootle <strong>Szabad "
"Szoftver</strong>, töltse le és használja sajátjaként, ha szeretné. A "
"fejlesztésben több féle módon is részt vehet (nincs szükség programozói "
"tudásra)."

#: indexpage.py:64
msgid ""
"The Pootle project itself is hosted at <a href=\"http://translate."
"sourceforge.net/\">translate.sourceforge.net</a> where you can find the "
"details about source code, mailing lists etc."
msgstr ""
"A Pootle projekt honlapja <a href=\"http://translate.sourceforge.net/"
"\">translate.sourceforge.net</a> helyen található, ahol részletek találhatók "
"a forráskódról, levelezőlistákról, egyebekről."

#. l10n: If your language uses right-to-left layout and you leave the English untranslated, consider enclosing the necessary text with <span dir="ltr">.......</span> to help browsers to display it correctly.
#. l10n: Take care to use HTML tags correctly. A markup error could cause a display error.
#: indexpage.py:67
msgid ""
"The name stands for <b>PO</b>-based <b>O</b>nline <b>T</b>ranslation / <b>L</"
"b>ocalization <b>E</b>ngine, but you may need to read <a href=\"http://www."
"thechestnut.com/flumps.htm\">this</a>."
msgstr ""
"A név jelentése <b>PO</b>-based <b>O</b>nline <b>T</b>ranslation / "
"<b>L</b>ocalization <b>E</b>ngine, /PO bázisú, Web alapú Fordító / Honosító "
"Motor/, de elolvashatja még <a "
"href=\"http://www.thechestnut.com/flumps.htm\">ezt</a> is."

#: indexpage.py:68
msgid "Versions"
msgstr "Verziók"

#. l10n: If your language uses right-to-left layout and you leave the English untranslated, consider enclosing the necessary text with <span dir="ltr">.......</span> to help browsers to display it correctly.
#. l10n: Take care to use HTML tags correctly. A markup error could cause a display error.
#: indexpage.py:71
#, python-format
msgid ""
"This site is running:<br />Pootle %s<br />Translate Toolkit %s<br />jToolkit "
"%s<br />Kid %s<br />ElementTree %s<br />Python %s (on %s/%s)"
msgstr ""
"Ezen a weboldalon: <br />Pootle %s<br />fordító eszközkészlet %s<br "
"/>jToolkit %s<br />Kid %s <br />ElemenTree %s<br />Python %s (%s/%s) fut"

#: indexpage.py:130
#, python-format
msgid "User Page for: %s"
msgstr "%s felhasználó oldala"

#: indexpage.py:132
msgid "Change options"
msgstr "Opciók változtatása"

#: indexpage.py:133
msgid "Admin page"
msgstr "Admin oldal"

#: indexpage.py:135
msgid "Quick Links"
msgstr "Gyors linkek"

#: indexpage.py:139
msgid "Please click on 'Change options' and select some languages and projects"
msgstr ""
"Kattintson az 'Opciók változtatása' gombra és válasszon valamilyen nyelvet "
"és projektet"

#: indexpage.py:192
#, python-format
msgid "%d project, average %d%% translated"
msgid_plural "%d projects, average %d%% translated"
msgstr[0] "%d projekt, átlag %d%% lefordítva"
msgstr[1] "%d projekt, átlag %d%% lefordítva"

#. l10n: The first parameter is the name of the installation
#. l10n: The second parameter is the name of the project/language
#. l10n: This is used as a page title. Most languages won't need to change this
#: indexpage.py:198 indexpage.py:260
#, python-format
msgid "%s: %s"
msgstr "%s: %s"

#: indexpage.py:214
msgid "Language Code"
msgstr "Nyelv kódja"

#: indexpage.py:215
msgid "Language Name"
msgstr "Nyelv neve"

#: indexpage.py:252
#, python-format
msgid "%d language, average %d%% translated"
msgid_plural "%d languages, average %d%% translated"
msgstr[0] "%d nyelv, átlag %d%% lefordítva"
msgstr[1] "%d nyelv, átlag %d%% lefordítva"

#: indexpage.py:265
msgid "Language"
msgstr "Nyelv"

#. l10n: The first parameter is the name of the installation (like "Pootle")
#: indexpage.py:348
#, python-format
msgid "%s: Project %s, Language %s"
msgstr "%s: Projekt %s, Nyelv %s"

#: indexpage.py:413
msgid "Cannot upload file, no file attached"
msgstr "Nem tudom feltölteni a fájlt, nincs fájl csatolva"

#: indexpage.py:419
msgid "Can only upload PO files and zips of PO files"
msgstr "Csak PO és PO-t tartalmazó zip fájlt tudok feltölteni"

#: indexpage.py:534
msgid "goals"
msgstr "cél"

#: indexpage.py:535
msgid "Enter goal name"
msgstr "Cél neve"

#: indexpage.py:536
msgid "Add Goal"
msgstr "Cél hozzáadása"

#: indexpage.py:541 indexpage.py:543
msgid "Upload File"
msgstr "Fájl feltöltése"

#: indexpage.py:542
msgid "Select file to upload"
msgstr "Feltöltendő fájl kiválasztása"

#. l10n: tooltip
#: indexpage.py:550
msgid "Overwrite the current file if it exists"
msgstr "Létező fájl felüírása"

#. l10n: radio button text
#: indexpage.py:552
msgid "Merge"
msgstr "Egyesítés"

#. l10n: tooltip
#: indexpage.py:554
msgid ""
"Merge the file with the current file and turn conflicts into suggestions"
msgstr "Egyesítse az új fájlt a régivel"

#: indexpage.py:657
msgid "Not in a goal"
msgstr "Nincs célban"

#: indexpage.py:677
msgid "Add User"
msgstr "Felhasználó hozzáadása"

#: indexpage.py:719
msgid "PO file"
msgstr "PO fájl"

#: indexpage.py:723
msgid "XLIFF file"
msgstr "XLIFF fájl"

#: indexpage.py:727
msgid "Qt .ts file"
msgstr "Qt .ts fájl"

#: indexpage.py:731
msgid "CSV file"
msgstr "CSV fájl"

#: indexpage.py:736
msgid "MO file"
msgstr "MO fájl"

#. l10n: Update from version control (like CVS or Subversion)
#: indexpage.py:742
msgid "Update"
msgstr "Aktualizál"

#: indexpage.py:785
msgid "All Strings"
msgstr "Minden mondat"

#: indexpage.py:787
msgid "Unassigned"
msgstr "Kiosztatlan"

#: indexpage.py:788
msgid "Unassigned and Untranslated"
msgstr "Kiosztatlan és fordítatlan"

#: indexpage.py:795
msgid "Set Goal"
msgstr "Cél beállítás"

#: indexpage.py:799
msgid "Select Multiple"
msgstr "Töbszörös kijelölés"

#: indexpage.py:801
msgid "Assign To"
msgstr "Kioszt"

#: indexpage.py:830
msgid "Show Editing Functions"
msgstr "Szerkesztési funkciót mutatása"

#: indexpage.py:831
msgid "Show Statistics"
msgstr "Statisztika mutatása"

#: indexpage.py:832
msgid "Show Tracks"
msgstr "Nyomkövetés mutatása"

#: indexpage.py:832
msgid "Hide Tracks"
msgstr "Nyomkövetés elrejtése"

#. l10n: "Checks" are quality checks that Pootle performs on translations to test for common mistakes
#: indexpage.py:834
msgid "Show Checks"
msgstr "Ellenőrzések megjelenítése"

#: indexpage.py:834
msgid "Hide Checks"
msgstr "Ellenőrzések elrejtése"

#: indexpage.py:835
msgid "Show Goals"
msgstr "Cél megjelenítése"

#: indexpage.py:835
msgid "Hide Goals"
msgstr "Cél elrejtése"

#: indexpage.py:836
msgid "Show Assigns"
msgstr "Megbízások mutatása"

#: indexpage.py:836
msgid "Hide Assigns"
msgstr "Megbízások elrejtése"

#: indexpage.py:845
#, python-format
msgid "All Goals: %s"
msgstr "Összes cél: %s"

#: indexpage.py:851
msgid "Translate My Strings"
msgstr "Mondataim fordítása"

#: indexpage.py:853
msgid "View My Strings"
msgstr "Mondataim megtekintése"

#: indexpage.py:858
msgid "No strings assigned to you"
msgstr "Nincsenek mondatok Önnek kiosztva"

#: indexpage.py:862
msgid "Quick Translate My Strings"
msgstr "Mondataim gyorsfordítása"

#: indexpage.py:866
msgid "No untranslated strings assigned to you"
msgstr "Nincsenek fordítatlan mondatok Önnek kiosztva"

#: indexpage.py:870
msgid "Review Suggestions"
msgstr "Javaslatok áttekintése"

#: indexpage.py:872
msgid "View Suggestions"
msgstr "Javaslatok megjelenítése"

#: indexpage.py:877
msgid "Quick Translate"
msgstr "Gyorsfordítás"

#: indexpage.py:879
msgid "View Untranslated"
msgstr "Fordítás nélküliek megjelenítése"

#: indexpage.py:883
msgid "No untranslated items"
msgstr "Nincs fordítatlan tétel"

#: indexpage.py:886
msgid "Translate All"
msgstr "Mindet lefordít"

#: indexpage.py:901
msgid "ZIP of goal"
msgstr "Célból ZIP"

#: indexpage.py:903
msgid "ZIP of folder"
msgstr "Könyvtárból ZIP"

#: indexpage.py:910
msgid "Generate SDF"
msgstr "SDF generálás"

#: indexpage.py:963
#, python-format
msgid "%d string (%d%%) failed"
msgid_plural "%d strings (%d%%) failed"
msgstr[0] "%d mondat (%d%%) hiányzik"
msgstr[1] "%d mondat (%d%%) hiányzik"

#: indexpage.py:993
#, python-format
msgid "%d/%d words (%d%%) assigned"
msgstr "%d/%d szó (%d%%) kiosztva"

#: indexpage.py:994 indexpage.py:996
#, python-format
msgid "[%d/%d strings]"
msgstr "[%d/%d mondat]"

#~ msgid "Broaden"
#~ msgstr "Szélesít"

#~ msgid "Narrow"
#~ msgstr "Keskeny"

#~ msgid "Reset"
#~ msgstr "Alaphelyzet"

#~ msgid "Must be a valid email address"
#~ msgstr "Valós e-mail cím szükséges"

#~ msgid "Input Width (in characters)"
#~ msgstr "Beviteli szélesség (karakterekben)"

#~ msgid "Pootle"
#~ msgstr "Pootle"

#~ msgid "Pootle: %s"
#~ msgstr "Pootle: %s"

#~ msgid ""
#~ "<strong>Pootle</strong> is a simple web portal that should allow you to "
#~ "<strong>translate</strong>!"
#~ msgstr ""
#~ "A <strong>Pootle</strong> egy egyszerű portál, aminek segítségével "
#~ "<strong>fordítani</strong> lehet!"

#~ msgid "parent folder"
#~ msgstr "szülő mappa"

#~ msgid "<h3 class=\"title\">Projects</h3>"
#~ msgstr "<h3 class=\"title\">Projektek</h3>"

#~ msgid "%d files, %d/%d strings (%d%%) translated"
#~ msgstr "%d fájl, %d/%d bejegyzés (%d%%) lefordítva"

#~ msgid "%d strings (%d%%) failed"
#~ msgstr "%d bejegyzés (%d%%) hibás"

#~ msgid "this is a demo installation of pootle"
#~ msgstr "ez a pootle bemutató célú telepítése"

#~ msgid "current folder"
#~ msgstr "jelenlegi mappa"

#~ msgid "login status"
#~ msgstr "bejelentkezési állapot"

#~ msgid "project root"
#~ msgstr "projekt kezdete"

#~ msgid "you do not have rights to suggest changes here"
#~ msgstr "nincs joga változtatást javasolni itt"

#~ msgid "you do not have rights to change translations here"
#~ msgstr "nincs joga a fordítást megváltoztatni itt"

#~ msgid "%d/%d strings (%d%%) translated"
#~ msgstr "%d/%d bejegyzés (%d%%) lefordítva"

#~ msgid "%d files, "
#~ msgstr "%d fájl,"

#~ msgid "<h3 class=\"title\">Languages</h3>"
#~ msgstr "<h3 class=\"title\">Nyelvek</h3>"

#~ msgid "current file"
#~ msgstr "jelenlegi fájl"

#~ msgid "not logged in"
#~ msgstr "nincs bejelentkezve"
