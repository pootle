Pootle: a web translation and translation management engine
===========================================================

* How it works
* References
* Requirement/Installation
* Running
* Bug reporting
* Notes
* Assignments

How it works
------------
The concept is: a PO file is a document, so lets use PO files directly, 
rather than storing it in a database.  But we need quick access to statistics 
so we cache them in .stats files. This lets us do most checks etc. in advance 
and then iterate quickly.

References
----------
Bugzilla: http://bugs.locamotion.org/
Wiki: http://translate.sourceforge.net/wiki/pootle/index
Mailing List: https://lists.sourceforge.net/lists/listinfo/translate-pootle
	translate-pootle@lists.sourceforge.net
Mail Archives: http://sourceforge.net/mailarchive/forum.php?forum=translate-pootle

Requirements/Installation
-------------------------
Please read http://translate.sourceforge.net/wiki/pootle/installation for the 
lastest instructions.

Pootle requires the Translate Toolkit package from 
http://translate.sourceforge.net/
Note all the optional dependencies of the Translate Toolkit for optimal 
performance and functionality in Pootle.

The Translate Toolkit requires Python 2.3 or higher, but Pootle itself might 
only work with Python 2.4 or higher.  The Translate Toolkit must be installed 
properly i.e. it needs to be accessible in your Python Path. This will normally
be accomplished by installing from source:

	python setup.py install
	
If you get this error along the lines of
Unable to open /usr/lib/python2.N/config/Makefile (no such file or directory)
while running setup.py, you need to install python-dev or libpython2.N-devel
package. Try to install python2.N-dev or libpython2.N-devel or something 
similar with your distribution's package manager.

For the web interface, Pootle requires jToolkit 
(http://jtoolkit.sourceforge.net/)
jToolkit can run applications either via Apache using mod_python, or standalone
from the command line.  Pootle works best at the moment being run from the 
command line since we don't handle inter-process locking etc.  It's also 
much easier! To install jToolkit run:

	tar xvzf jToolkit-N.N.N.tar.gz
	cd jToolkit-N.N.N
	python jToolkitSetup.py install

ZIP is needed to enable users to download ZIP files of directories. For 
integration with CVS and/or Subversion, the relevant version control client 
must be installed and accessible from the PATH.

From version 0.9 of Pootle, kid (http://kid.lesscode.org/), and elementtree
(http://effbot.org/downloads/#elementtree) are also needed.  Elementtree is
not needed anymore with Python 2.5.

To install "elementtree", run
	tar xvfz elementtree-N.N.N.tar.gz
	cd elementtree-N.N.N
	python setup.py install
	
To install kid, run
	tar xvfz kid-N.N.N.tar.gz
	cd kid-N.N.N
	python setup.py install
  
For Debian users: if you do not have setuptools installed, try:
	apt-get install python2.N-setuptools
or
	wget http://files.turbogears.org/eggs/setuptools-0.6a11-py2.4.egg

Installation of PyLucene is optional, but helps to speed up searching. jToolkit 
is not yet compatible with the newest version of PyLucene, so be sure to use 
a version before PyLucene version 2.0.

To install Pootle itself, run

	tar xzf Pootle-N.N.N.tar.gz
	cd Pootle-N.N.N
	python pootlesetup.py install

Running
-------
To run, edit pootle.prefs and specify your languages and projects. The key detail 
here is the podir for each project.  Then run:

	PootleServer

You can also copy pootle.prefs to another directory, allowing you to run Pootle outside
of the installation directory.  Edit this copy of pootle.prefs as needed.  Then run:

	PootleServer --prefsfile=your.prefs

Use --help to see the other options.  You can tell it which prefs file to use, 
what HTML file and what port to run on (the default is 8080). The defaults will 
generally work.

Now visit http://localhost:8080/ and begin translating.

To stop, press Ctrl-C. On Windows, it will only react to the Ctrl-C after a webpage is requested.

Bug Reporting/Feature requests
------------------------------
You can always report bugs or feature requests on the mailing list but because of the increase
in users and the fact that bug reports do go missing it is probably best to place your
bug report in Bugzilla: http://bugs.wordforge.org/

If you have a traceback or a patch then please include it.  Also please be quite specific
about how the bug occured and what you expected to happen.

If this is a feature request then try to be specific about how you think this feature should 
work.

Notes
-----
Files should be reindexed automatically.
To reindex manually, simply remove any stats files you want reindexed, or run 
	python pootle.py --refreshstats

Assignments
-----------
To assign sstrings to users, you need to create a xxx.po.assigns file in the same 
directory as xxx.po This should contain lines in the following format:

username: action: 0,2-34,36,99

where action can be 'suggest' or 'review', and the numbers refer to 0-based item 
numbers in the file.

